function r = p3 (n)
    r = [1 1];
    for i = 3:n
        r(i) = r(i-1) + r(i-2);
    end
end

