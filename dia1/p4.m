function r = p4(n)
    r = ones(1,n);
    for i = 3:n
        r(i) = r(i-1) + r(i-2);
    end
end

