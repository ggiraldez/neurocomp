function iext()

E = -70;
V_thresh = -55;
V_reset = -80;
c_m = 10;
r_m = 1;
A = 0.025;
V_peak = 40;
R_m = r_m / A;
tau = r_m * c_m;

% dt = diferencial de tiempo
% N = cantidad de muestras

% 10000 muestras con dt = 0.1 ms totalizan 1 seg
N = 10000;
dt = 0.1; % ms
T = N * dt; % tiempo total en ms
I = pulsoCuadrado(0.5, 2500, 7500, 10000);
V1 = calcularV(dt, I);

% dividimos por 1000 el tiempo para que el rate sea en Hz (picos por
% segundo)
rate = calcularRate(V1, T / 1000)

% graficar V
figure();
plot([1:N] ./ N * T, V1, 'g-');
xlabel('tiempo (ms)');
ylabel('V (mV)');

rates = [];
for j = 1:100
    I = pulsoCuadrado(0.01*j, 2500, 7500, 10000);
    V = calcularV(dt, I);
    rates(j) = calcularRate(V, T/1000);
end
figure();
plot(rates, '+');
rates

end
function V = calcularV(dt, I_ext)
    E = -70;
    V_thresh = -55;
    V_reset = -80;
    c_m = 10;
    r_m = 1;
    A = 0.025;
    V_peak = 40;
    R_m = r_m / A;
    tau = r_m * c_m;

    N = size(I_ext, 2);
    V = zeros(1,N);

    V(1) = E;
    for i = 2:N
        if V(i-1) > V_thresh
            V(i-1) = V_peak;
            V(i) = V_reset;
        else
            dV = (E - V(i-1) + R_m * I_ext(i)) / tau;
            V(i) = V(i-1) + dt * dV;
        end
    end
end

% buscar los elementos donde hay picos y contarlos
function r = calcularRate(V1, T)
    V_peak = 40;
    r = size(find(V1 >= V_peak),2) / T;
end

% I_max valor del pulso
% N1 nro. de muestra donde empieza el pulso
% N2 nro. de muestra donde termina
% N cantidad total de muestras
% N1 < N2 < N
function I = pulsoCuadrado(I_max, N1, N2, N)
    I = [zeros(1,N1) ones(1,N2-N1).*I_max zeros(1,N-N2)];
end


