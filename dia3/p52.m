load('MI_MAC_CenterOutTrain_Hatsopolous_1direction.mat');

figure
hold on

for y = 1:size(spike,2)
    rasterplot(spike(y).times, y*0.2)
end

hold off
 