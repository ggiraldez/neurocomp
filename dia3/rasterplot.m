function rasterplot(data, y)
%RASTERPLOT Summary of this function goes here
%   Detailed explanation goes here

    rp_height = 0.1;
    xs = [];
    ys = [];
    for i = 1:size(data,1)
        xs(2*i-1) = data(i);
        xs(2*i) = data(i);
        ys(2*i-1) = y;
        ys(2*i) = y+rp_height;
        %line([data(i) data(i)], [y y+rp_height], 'color', 'b');
    end
    %line(xs, ys, 'Color', 'b')
    plot(data, y*ones(size(data)), '+', 'markersize', 18);
    line([-1 1], [y y], 'color', 'w');
    plot(data, y*ones(size(data)), '.', 'markersize', 1);

end

