function refract()
    N = 1000;
    T = 1;  % en segundos
    dt = T/N*1000; % en ms
    I = sinusoidal(5, N, T, 4);
    V = calcularV(dt, I);
    xlabel('tiempo (ms)');
    ylabel('V');
    plot([1:N] .* dt, V);
end

function firingRateEnFuncionDeI()
    rates = [];
    for j = 1:100
        rates(j) = firingRate(j*0.01);
    end
    figure();
    plot([1:100] .* 0.01, rates, '*')
    xlabel('corriente (nA)')
    ylabel('firing rate')
end

function V = calcularV(dt, I_ext)
    E = -70;
    V_thresh = -55;
    V_reset = -80;
    c_m = 10;
    r_m = 1;
    A = 0.025;
    V_peak = 40;
    R_m = r_m / A;
    tau = r_m * c_m;

    N = size(I_ext, 2);
    V = zeros(1,N);

    t_refract = 0;
    t_ref = 2;

    V(1) = E;
    for i = 2:N
        t_refract = t_refract + dt;
        if(t_refract > t_ref)
            if V(i-1) > V_thresh
                V(i-1) = V_peak;
                V(i) = V_reset;
                t_refract = 0;
            else
                dV = (E - V(i-1) + R_m * I_ext(i)) / tau;
                V(i) = V(i-1) + dt * dV;
            end
        else
            V(i) = V(i-1);
        end
    end
end

% buscar los elementos donde hay picos y contarlos
function r = calcularRate(V1, T)
    V_peak = 40;
    r = size(find(V1 >= V_peak),2) / T;
end

% I_max valor del pulso
% N1 nro. de muestra donde empieza el pulso
% N2 nro. de muestra donde termina
% N cantidad total de muestras
% N1 < N2 < N
function I = pulsoCuadrado(I_max, N1, N2, N)
    I = [zeros(1,N1) ones(1,N2-N1).*I_max zeros(1,N-N2)];
end

% I_max valor de amplitud máxima de corriente
% N cantidad de muestras
% T tiempo total de la muestra
% f frecuencia del seno
function I = sinusoidal(I_max, N, T, f)
    x = [1:N] .* 2 * pi * f * T / N;
    I = [sin(x)] .* I_max;
end

function R = firingRate(Corriente)
    T = 1; %1 segundo de muestra
    M = 10000; %muestras
    dt = (T / M) * 1000; %en ms
    I = pulsoCuadrado(Corriente, 1, M, M);
    V = calcularV(dt, I);
    R = calcularRate(V, T);
end

