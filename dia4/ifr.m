% dt paso de tiempo
% I_ext vector de corriente de excitación (dada con paso dt)
function V = ifr(dt, I_ext)
    E = -70;
    V_thresh = -55;
    V_reset = -80;
    c_m = 10;
    r_m = 1;
    A = 0.025;
    V_peak = 40;
    R_m = r_m / A;
    tau = r_m * c_m;

    N = size(I_ext, 2);
    V = zeros(1,N);

    t_refract = 0;
    t_ref = 2;

    V(1) = E;
    for i = 2:N
        t_refract = t_refract + dt;
        if t_refract > t_ref
            if V(i-1) > V_thresh
                V(i-1) = V_peak;
                V(i) = V_reset;
                t_refract = 0;
            else
                dV = (E - V(i-1) + R_m * I_ext(i)) / tau;
                V(i) = V(i-1) + dt * dV;
            end
        else
            V(i) = V(i-1);
        end
    end
end

