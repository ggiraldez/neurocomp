% dt paso de tiempo
% I_ext vector de corriente de excitación (dada con paso dt)
function [v u] = izh(dt, I_ext, a, b, c, d)
    E = -70;
    V_thresh = 30;
    V_peak = 30;
    c_m = 10;
    r_m = 1;
    A = 0.025;
    R_m = r_m / A;
    tau = r_m * c_m;

    N = size(I_ext, 2);
    v = zeros(1,N);
    u = zeros(1,N);

    v(1) = E;
    u(1) = b * v(1);
    for i = 2:N
        if v(i-1) >= V_thresh
            v(i-1) = V_peak;
            v(i) = c;
            u(i) = u(i-1) + d;
        else
            dV = 0.04 * v(i-1) * v(i-1) + 5 * v(i-1) + 140 - u(i-1) + I_ext(i);
            dU = (a * (b * v(i-1) - u(i-1)));
            v(i) = v(i-1) + dV * dt;
            u(i) = u(i-1) + dU * dt;
        end
    end
end


