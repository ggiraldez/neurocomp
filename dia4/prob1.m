N = 1000;
dt = 1;
t = [1:N];

I = 10 * ones(1, N);
[v u] = izh(dt, I, 0.02, 0.2, -65, 8);
plot(t, v, t, u);
pause;

I = 10 * ones(1, N);
[v u] = izh(dt, I, 0.02, 0.2, -55, 4);
plot(t, v, t, u);
pause;

I = 10 * ones(1, N);
[v u] = izh(dt, I, 0.02, 0.2, -50, 2);
plot(t, v, t, u);
pause;

I = 10 * ones(1, N);
[v u] = izh(dt, I, 0.1, 0.2, -65, 2);
plot(t, v, t, u);
pause;

I = -99 * ones(1, N);
[v u] = izh(dt, I, 0.2, 2, -56, -16);
plot(t, v, t, u);

