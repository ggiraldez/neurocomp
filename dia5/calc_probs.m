function [Pr, Ps, Prs] = calc_probs(samples)
    % r=1  -> 0 < FR <= 20
    % r=2  -> 20 <= FR <= 30
    % r=3  -> 30 <= FR

    fr = cell2mat(struct2cell(samples)(4,:));
    dir = cell2mat(struct2cell(samples)(1,:));

    nr = columns(fr);

    Pr = [];
    Pr(1) = columns(find(fr <= 20)) / nr;
    Pr(2) = columns(find(20 < fr & fr <= 30)) / nr;
    Pr(3) = columns(find(fr > 30)) / nr;

    Ps = [];
    Prs = zeros(3,8);
    for d = 1:8
        indexes = find(dir == d);
        nr_dir = columns(indexes);
        Ps(d) = nr_dir / nr;

        fr_dir = fr(indexes);
        Prs(1,d) = columns(find(fr_dir <= 20)) / nr;
        Prs(2,d) = columns(find(20 < fr_dir & fr_dir <= 30)) / nr;
        Prs(3,d) = columns(find(fr_dir > 30)) / nr;
    end
end

