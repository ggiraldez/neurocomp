function [Pr, Ps, Prs] = calc_probs2(R, dir)
    nr = rows(R);

    Pr = zeros(27,1);
    for i = 1:nr
        id = code(R(i,:));
        Pr(id) = Pr(id) + 1;
    end
    Pr = Pr ./ nr;

    Ps = zeros(8,1);
    Prs = zeros(27,8);
    for d = 1:8
        indexes = find(dir == d);
        nr_dir = rows(indexes);
        Ps(d) = nr_dir / nr;

        R_dir = R(indexes,:);
        for i = 1:nr_dir
            id = code(R_dir(i,:));
            Prs(id,d) = Prs(id,d) + 1;
        end
    end
    Prs = Prs ./ nr;
end


