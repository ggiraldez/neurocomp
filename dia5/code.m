function id = code(r)
    % codificar una tripla de firing-rates cuantizados
    id = (r(1)-1) + (r(2)-1) * 3 + (r(3)-1) * 9 + 1;
end

