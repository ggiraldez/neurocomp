function r = decode(id)
    % encontrar la tripla de firing-rates cuantizados asociada al código
    r = [];
    r(1) = mod(id-1, 3) + 1;
    r(2) = mod(id - r(1), 9) / 3 + 1;
    r(3) = mod(id - r(1) - 3*(r(2)-1), 27) / 9 + 1;
end

