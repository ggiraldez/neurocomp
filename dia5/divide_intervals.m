function frs = divide_intervals(samples)
    % dividir los trenes de spikes en 3 intervalos y calcular
    % los firing-rates de cada uno

    frs = zeros(columns(samples), 3);
    for i = 1:columns(samples)
        spikes = samples(i).spikes;
        tmin = min(spikes);
        tmax = max(spikes);
        frs(i, 1) = rows(find(spikes < 0)) / (0 - tmin);
        frs(i, 2) = rows(find(spikes >= 0 & spikes < 1)) / (1 - 0);
        frs(i, 3) = rows(find(spikes >= 1)) / (tmax - 1);
    end
end

