function [Hr, Hs, Hr_s] = entropy(Pr, Ps, Prs)
    Hr = -sum(Pr .* log2(Pr));
    Hs = -sum(Ps .* log2(Ps));

    % probabilidad condicional P[r/s] a partir de P[r,s]
    Pss = [Ps;Ps;Ps];
    Pr_s = Prs ./ Pss;

    Hr_s = 0;
    for i = 1:prod(size(Prs))
        if Prs(i) ~= 0
            Hr_s = Hr_s - Pss(i) * Pr_s(i) * log2(Pr_s(i));
        end
    end
end


