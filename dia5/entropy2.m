function [Hr, Hs, Hr_s] = entropy2(Pr, Ps, Prs)
    Pr2 = Pr(find(Pr ~= 0));
    Hr = -sum(Pr2 .* log2(Pr2));

    Hs = -sum(Ps .* log2(Ps));

    % probabilidad condicional P[r/s] a partir de P[r,s]
    Pr_s = Prs;
    for i = 1:rows(Prs)
        Pr_s(i,:) = Prs(i,:) ./ Ps';
    end

    Hr_s = 0;
    for i = 1:prod(size(Prs))
        if Prs(i) ~= 0
            Hr_s = Hr_s - Prs(i) * log2(Pr_s(i));
        end
    end
end


