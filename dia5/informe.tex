\documentclass[a4paper, 11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{caption}
\usepackage{subcaption}
\usepackage[spanish]{babel}

\addto\captionsspanish{\renewcommand{\tablename}{Tabla}}

\begin{document}

\title{Herramientas Computacionales en Neurociencias \\ 
Laboratorio 5: Teoría de la información y neural coding}
\author{Gustavo Giráldez}
\date{Enero 2013}
\maketitle

\section*{Problema 1}

El archivo \texttt{MI\_MAC\_CenterOutTrainTest\_Hatsopolous.mat} contiene la
información de trenes de spikes registrados en la corteza primaria motora de un
macaco, durante una serie de 315 experimentos denominados \emph{center-out
paradigm}, consistente en mover una palanca a una de 8 direcciones posibles. Se
definen cuatro variables:

\begin{itemize}

\item \textbf{unit}, un vector de $1 \times 143$ estructuras con información
sobre los spikes de 143 neuronas. Cada estructura consiste en un vector de
tamaño variable \textbf{times} con los instantes de tiempo de ocurrencia de
cada spike, y \textbf{chanNum}, \textbf{unitNum} y \textbf{area}, todas
constantes que describen el canal de sensado de la neurona.

\item \textbf{instruction}, un vector de $315 \times 1$ con los tiempos de
inicio de los 315 experimentos.

\item \textbf{go}, un vector de $315 \times 1$ con los tiempos de la orden
\emph{go} para los experimentos.

\item \textbf{direction}, un vector de $315 \times 1$ con la dirección asociada
a cada uno de los experimentos.

\end{itemize}


\subsection*{\emph{Raster plots}}

Para cada neurona, podemos entonces extraer información de los trenes de spikes
medidos en cada experimento tomando una ventana de tiempo que se extienda desde
el tiempo \emph{instruction} hasta unos segundos después de la orden \emph{go}
asociada. Para facilitar el análisis posterior, corregimos el tiempo de los
spikes tal que la orden \emph{go} ocurra en tiempo 0 para cada experimento (ver
script \texttt{process\_data.m}).

Mediante un análisis rápido de los vectores \textbf{instruction} y \textbf{go}
elegimos una ventana de 2 segundos luego del \emph{go} para recortar las
muestras de trenes de spikes para cada experimento. De esta forma podemos
graficar el \emph{raster plot} y realizar un PSTH (peri-stimulus time histogram)
para la neurona 16, discriminando cada una de las 8 direcciones (ver figura
\ref{fig:p1a}, script \texttt{p1a.m}).

\begin{figure}[t]
\centering
\subfloat[Dirección 1]{\includegraphics[scale=0.15]{p1a-1.png}}
\subfloat[Dirección 2]{\includegraphics[scale=0.15]{p1a-2.png}}
\\
\subfloat[Dirección 3]{\includegraphics[scale=0.15]{p1a-3.png}}
\subfloat[Dirección 4]{\includegraphics[scale=0.15]{p1a-4.png}}
\\
\subfloat[Dirección 5]{\includegraphics[scale=0.15]{p1a-5.png}}
\subfloat[Dirección 6]{\includegraphics[scale=0.15]{p1a-6.png}}
\\
\subfloat[Dirección 7]{\includegraphics[scale=0.15]{p1a-7.png}}
\subfloat[Dirección 8]{\includegraphics[scale=0.15]{p1a-8.png}}

\caption{PSTH y \emph{Raster plot} para cada dirección para la neurona 16
(script \texttt{p1a\_print.m}).} \label{fig:p1a}
\end{figure}


\subsection*{Función densidad de spikes}

Realizando una convolución con un kernel gaussiano podemos convertir un tren de
spikes en una función continua densidad de spikes. En el caso de la figura
\ref{fig:p1b}, se utilizó un kernel con media 0 y desviación estándar 15 ms,
para una muestra particular (script \texttt{p1b.m}).

\begin{figure}[t]
\centering
\includegraphics[trim={6cm 0 4cm 0},clip,scale=0.35]{p1b-5.png}
\caption{\emph{Spike density function} de la neurona 16, a
partir del \emph{raster plot} usando un kernel gaussiano (script
\texttt{p1b.m}).} \label{fig:p1b}
\end{figure}


\subsection*{Entropía e información mutua}

Considerando la dirección de cada una de las muestras como la variable estímulo
$S$ y dividiendo al spike count en tres categorías, podemos calcular las
probabilidades $P[r]$, $P[s]$ y $P[r,s]$. Consideraremos las categorías:

\begin{center}
    \begin{tabular}{l l}
    $r$ & \emph{firing rate} \\
    \hline
    $1$ & $0 < FR <= 20$  \\
    $2$ & $20 < FR <= 30$ \\
    $3$ & $30 <= FR$      \\
    \end{tabular}
\end{center}

Realizaremos dos análisis diferentes, que implican dos significados o
interpretaciones distintas de la variable $R$:

\begin{itemize}

\item tomando y categorizando el \emph{firing rate} del intervalo completo de
la muestra (ie. desde el tiempo \emph{instruction} hasta 2 segundos después
del \emph{go},

\item dividiendo el intervalo de la muestra en 3 subintervalos, desde
\emph{instruction} hasta \emph{go} (tiempo 0), desde 0 a 1 segundo y desde 1 a 2
segundos, y calculando y categorizando el \emph{firing rate} para cada
subintervalo.

\end{itemize}

En ambos casos trabajaremos sobre las muestras tomadas en la neurona 16.


\subsubsection*{\emph{Firing rate} de la muestra completa}

Las probabilidades calculadas tomando un único intervalo (muestra completa)
para los distintos valores de $S$ (dirección) y $R$ (cada categoría de
\emph{firing rate}) pueden verse en la tabla \ref{tab:p1c} (script
\texttt{p1c.m}).

\begin{table}[ht!]

\begin{subtable}{0.2\textwidth}
    \centering
    \begin{tabular}{l | r} 
      & $P[r]$ \\
    \hline
    1 & 0.362 \\
    2 & 0.533 \\
    3 & 0.105 \\
    \end{tabular}
    \caption{$P[r]$}
\end{subtable}
~
\begin{subtable}{0.8\textwidth}
    \centering
    \begin{tabular}{c | c c c c c c c c}
           & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
    \hline
    $P[s]$ & 0.092 & 0.105 & 0.146 & 0.149 & 0.143 & 0.140 & 0.137 & 0.089 \\
    \end{tabular}
    \caption{$P[s]$}
\end{subtable}

\vspace{3mm}

\begin{subtable}{\textwidth}
    \centering
    \begin{tabular}{c | c c c c c c c c} 
    $P[r,s]$ & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 \\
    \hline
    1 & 0.06032 & 0.06984 & 0.01270 & 0.03492 & 0.02857 & 0.01905 & 0.08254 & 0.05397 \\
    2 & 0.03175 & 0.03175 & 0.08889 & 0.10476 & 0.08889 & 0.11429 & 0.04762 & 0.02540 \\
    3 & 0.00000 & 0.00317 & 0.04444 & 0.00952 & 0.02540 & 0.00635 & 0.00635 & 0.00952 \\
    \end{tabular}
    \caption{$P[r,s]$}
\end{subtable}

\caption{Probabilidades obtenidas al categorizar el \emph{firing rate} del
total del intervalo de cada muestra (script \texttt{p1c.m}).}
\label{tab:p1c}

\end{table}

A partir de esas probabilidades, estimamos las entropías y la información
mútua (script \texttt{p1d.m}):

\begin{align*}
H[r] &= 1.3553 \\
H[s] &= 2.9729 \\
H[r|s] &= 1.1286 \\
I[r,s] &= 0.22668
\end{align*}

Como puede apreciarse de los valores encontrados no existe prácticamente
correlación entre estímulo y respuesta, ya que la entropía de ruido $H[r|s]$ es
casi igual a la entropía de la respuesta $H[r]$, dejando aproximadamente un
quinto de bit de información extraíble del análisis de la respuesta.

Adicionalmente, por la forma en que analizamos el \emph{firing rate}, es
imposible codificar las 8 direcciones de los experimentos. La entropía de la
respuesta $H[r]$ es aún menor que los $1.58$ bits necesarios para codificar las
3 categorías de \emph{firing rate} seleccionadas, lo cual indica que hay
categorías que son preferidas por la neurona, independientemente del estímulo.

El valor $H[s]$ es aproximadamente 3 que es lo esperable para una distribución
uniforme de probabilidad para las 8 direcciones de las muestras.


\subsubsection*{\emph{Firing rate} de 3 intervalos de cada muestra}

Dividiendo el tren de spikes de cada muestra en 3 subintervalos, desde
\emph{instruction} hasta 0 (que representa la orden \emph{go}), desde 0 a 1
segundo, y de 1 segundo en adelante, y categorizando el \emph{firing rate} en
cada intervalo, podemos recalcular las probabilidades, estimar las entropías y
la información mútua.

En este caso, codificaremos la respuesta $R$ según la categorización del
\emph{firing rate} de cada subintervalo. Por ejemplo entonces, $r = 123$
codifica una respuesta con \emph{firing rate} entre 0 y 20 para el primer
intervalo, entre 20 y 30 para el segundo y mayor a 30 para el tercero.

Las probabilidades calculadas se pueden ver en la tabla \ref{tab:p1c2} y en la
figura \ref{fig:p1c2mesh}. Desde luego, los valores para $P[s]$ son idénticos
al caso anterior (script \texttt{p1c2.m}).

\begin{table}[ht!]
\centering
\begin{tabular}{c | c c c}
$P[r]$ & $x=1$ & $x=2$ & $x=3$ \\
\hline
$x11$ & 0.102 & 0.111 & 0.060 \\
$x21$ & 0.067 & 0.089 & 0.054 \\
$x31$ & 0.000 & 0.010 & 0.022 \\
$x12$ & 0.073 & 0.041 & 0.029 \\
$x22$ & 0.019 & 0.083 & 0.070 \\
$x32$ & 0.010 & 0.013 & 0.006 \\
$x13$ & 0.013 & 0.010 & 0.010 \\
$x23$ & 0.019 & 0.022 & 0.038 \\
$x33$ & 0.000 & 0.003 & 0.029 \\
\end{tabular}
\caption{Probabilidad de la respuesta codificada para una división en 3
intervalos de cada muestra (script \texttt{p1c2.m}).} \label{tab:p1c2}
\end{table}

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.3]{p1c2-mesh.png}
\caption{Probabilidad de la respuesta en función de la dirección de estímulo,
para una división de 3 intervalos en cada muestra (script
\texttt{p1c2\_mesh.m}).} \label{fig:p1c2mesh}
\end{figure}

Estos valores de probabilidad dan como resultado las siguientes estimaciones de
entropía e información mútua:

\begin{align*}
H[r] &= 4.1835 \\
H[s] &= 2.9729 \\
H[r|s] &= 3.2386 \\
I[r,s] &= 0.94484
\end{align*}

Con esta nueva forma de analizar la información nos aseguramos que sea posible
(al menos teóricamente) identificar las 8 direcciones a partir de la respuesta.
Como categorizamos 3 intervalos en 3 categorías diferentes, a priori tenemos 27
posibles combinaciones. Experimentalmente sin embargo, dado el valor hallado de
$H[r]$ y mirando la figura \ref{fig:p1c2mesh} observamos que no todas las
combinaciones ocurren. Según el valor de entropía, tenemos poco más de 4 bits
de información, o aproximadamente 18 combinaciones.

Desde luego, la entropía de ruido $H[r|s]$ también es más elevada, pero la
información mútua estimada ahora es prácticamente 1 bit, con lo cual sería
posible identificar dos particiones en el conjunto de direcciones estímulo a
partir de analizar la respuesta.


\section*{Problema 2}

Una forma de reducir el \emph{bias} en las mediciones del experimento consiste
en reordenar o mezclar aleatoriamente las muestras de los intervalos de
categorización de la respuesta. Esto tiene el efecto de eliminar la correlación
entre la respuesta en los diferentes intevalos.

El procedimiento entonces consiste en tomar para cada estímulo (dirección)
todas las respuestas por cada intervalo y mezclarlas aleatoriamente sin
repeticiones. Hecho esto obtenemos una nueva entropía de ruido $H_{sh}$
ligeramente mayor, lo reduce un poco la información
mútua (script \texttt{p2.m}):

\begin{align*}
H_{sh}[r|s] &\approx 3.38 \quad (\sigma = 0.035) \\
I_{sh}[r,s] &\approx 0.8
\end{align*}


\section*{Problema 3}

El \emph{bias} por la cantidad limitada de muestras puede apreciarse claramente
utilizando una neurona no informativa que genere un número aleatorio de spikes
en cada intervalo sin importar la dirección estímulo (script \texttt{p3.m}).

En la figura \ref{fig:p3} se puede ver la estimación de la entropía de la
respuesta $H[r]$ y del ruido $H[r|s]$ para una neurona de estas características
para un número creciente de muestras.

\begin{figure}[t]
\centering
\includegraphics[scale=0.35]{p3.png}
\caption{Entropía de la respuesta y de ruido para una neurona no informativa
(\emph{spike rate} aleatorio en los 3 intervalos para las 8 direcciones) en
función de número de muestras (script \texttt{p3.m}).} \label{fig:p3}
\end{figure}

Como no existe correlación directa entre el estímulo y la respuesta, lo
esperable es que la información mútua ($I[r,s] = H[r] - H[r|s]$) sea 0. Sin
embargo, por efecto del bias por la limitada cantidad de muestras, puede
apreciarse en el gráfico que no es el caso, y que la entropía de ruido converge
asintóticamente a la entropía de la respuesta para un número infinito de
muestras.

\end{document}

