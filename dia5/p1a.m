process_data;

figure;
for dir = 1:8
    subplot(4,2,dir);
    plot_samples(samples, dir, 2);
end

figure;
for dir = 1:8
    subplot(4,2,dir);
    plot_psth(samples, dir);
end

