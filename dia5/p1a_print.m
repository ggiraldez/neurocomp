process_data;

for dir = 1:8
    figure;
    title(sprintf('Dirección %d', dir));
    subplot(2,1,1);
    plot_psth(samples, dir);
    subplot(2,1,2);
    plot_samples(samples, dir, 8);
    print(sprintf('p1a-%d.png', dir));
end

