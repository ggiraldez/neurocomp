process_data;

samplenr = 5;
spikes = samples(samplenr).spikes;
dir = samples(samplenr).direction;

[t, sd] = spike_density(spikes);

figure;
hold on;
rasterplot(spikes, 20, 18);
plot(t, sd, 'r');
title(sprintf('Densidad de spikes, muestra #%d, dirección %d', samplenr, dir));
axis([-2 2]);
axis('labelx');
hold off;

print(sprintf('p1b-%d.png', samplenr), '-S1500,500');

