process_data;

[Pr, Ps, Prs] = calc_probs(samples);

for i = 1:columns(Pr)
    disp(sprintf('Pr[%d] = %.3f', i, Pr(i)));
end

for d = 1:8
    disp(sprintf('Ps[%d] = %.3f', d, Ps(d)));
end

disp('P[r,s] = ');
disp(Prs);


