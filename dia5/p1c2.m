process_data;

% calcular la matriz de respuestas (los trenes de spikes divididos en
% intervalos y cuantizados)
R = quantize(divide_intervals(samples));
% obtener el vector columna con las direcciones
dir = cell2mat(struct2cell(samples)(1,:))';

[Pr, Ps, Prs] = calc_probs2(R, dir);

for i = 1:rows(Pr)
    r = decode(i);
    disp(sprintf('Pr[%s] = %.3f', mat2str(r), Pr(i)));
end

for d = 1:8
    disp(sprintf('Ps[%d] = %.3f', d, Ps(d)));
end

disp('P[r,s] = ');
disp(Prs);


