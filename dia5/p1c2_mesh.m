process_data;

% calcular la matriz de respuestas (los trenes de spikes divididos en
% intervalos y cuantizados)
R = quantize(divide_intervals(samples));
% obtener el vector columna con las direcciones
dir = cell2mat(struct2cell(samples)(1,:))';

[Pr, Ps, Prs] = calc_probs2(R, dir);

mesh([1:8], [1:27], Prs);
xlabel('s (dirección)');
ylabel('r');
zlabel('P[r,s]');
axis([1 8 1 27]);
view(60,75);

print('p1c2-mesh.png');


