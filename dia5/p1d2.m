process_data;

% calcular la matriz de respuestas (los trenes de spikes divididos en
% intervalos y cuantizados)
R = quantize(divide_intervals(samples));
% obtener el vector columna con las direcciones
dir = cell2mat(struct2cell(samples)(1,:))';

[Pr, Ps, Prs] = calc_probs2(R, dir);

[Hr, Hs, Hr_s] = entropy2(Pr, Ps, Prs);

Hr
Hs
Hr_s

Ir_s = Hr - Hr_s

