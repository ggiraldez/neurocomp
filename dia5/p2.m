process_data;

% calcular la matriz de respuestas (los trenes de spikes divididos en
% intervalos y cuantizados)
R = quantize(divide_intervals(samples));
% obtener el vector columna con las direcciones
dir = cell2mat(struct2cell(samples)(1,:))';

Hsh = [];
for i = 1:20
    % mezclar la matriz de respuesta (al azar, sin repetición y dentro del mismo
    % estímulo)
    Rsh = shuffle(R, dir);

    [Pr, Ps, Prs] = calc_probs2(Rsh, dir);
    [_, _, Hsh(i)] = entropy2(Pr, Ps, Prs);
end

Hsh

