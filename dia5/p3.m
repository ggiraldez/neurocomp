min_samples = 50;
max_samples = 3000;
count_samples = 200;
Hr = zeros(count_samples, 1);
Hs = zeros(count_samples, 1);
Hr_s = zeros(count_samples, 1);
samples = ceil([1:count_samples] .* \
    (max_samples - min_samples) / count_samples + min_samples);

for k = 1:count_samples
    nr_samples = samples(k);

    R = ceil(rand(nr_samples,3) * 3);
    dir = ceil(rand(nr_samples,1) * 8);

    [Pr, Ps, Prs] = calc_probs2(R, dir);
    [Hr(k), Hs(k), Hr_s(k)] = entropy2(Pr, Ps, Prs);
end

figure;
hold on;
plot(samples, Hr, 'b');
plot(samples, Hs, 'g');
plot(samples, Hr_s, 'r');
legend('H[r]', 'H[s]', 'H[r|s]', 'location', 'southeast');
title('Entropía de una neurona no informativa');
xlabel('Cantidad de muestras');
hold off;

print('p3.png');

