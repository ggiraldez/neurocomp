function plot_psth(samples, dir)
    dt = 0.05;
    spikes = [];
    for s = 1:columns(samples)
        if samples(s).direction == dir
            spikes = [spikes; samples(s).spikes];
        end
    end
    tt = [min(spikes):dt:max(spikes)];
    [nn, xx] = hist(spikes, tt);
    bar(xx, nn, 1, 'linestyle', 'none', 'edgecolor', 'k', 'facecolor', 'k');
    xlabel('t');
    axis([-2.1 2.1 0 100]);
end

