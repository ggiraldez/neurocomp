function plot_samples(samples, dir, marker)
    hold on;
    xlabel('t');
    ylabel('trials');
    
    y = 0;
    for s = 1:columns(samples)
        if samples(s).direction == dir
            rasterplot(samples(s).spikes, y, marker);
            y = y + 1;
        end
    end
    axis([-2 2 -0.5 y-0.5]);
    axis('tic[x]');
    hold off;
end


