clear;
data = load('MI_MAC_CenterOutTrainTest_Hatsopolous.mat');

% el índice de la neurona que vamos a analizar
neuron = 16;
% el intervalo de tiempo luego del go-time que consideramos como parte de la
% muestra
interval = 2;

times = data.unit(neuron).times;
samples = [];

% procesar los datos de entrada normalizando los tiempos de las muestras
for i = 1:rows(data.instruction)
    it = data.instruction(i);
    gt = data.go(i);

    % buscar los spikes que caen dentro del intervalo de la muestra
    indexes = find(times >= it & times < gt + interval);

    % normalizar los tiempos de spike a go-time igual a cero
    spikes = [];
    for j = 1:rows(indexes)
        spikes(j) = times(indexes(j)) - gt;
    end

    % datos de la muestra
    samples(i).direction = data.direction(i);
    samples(i).spikes = spikes';

    % cantidad de spikes y firing-rate
    samples(i).total_count = columns(spikes);
    samples(i).fr = samples(i).total_count / (gt + interval - it);

    % spikes y firing-rate, a partir del go cue
    samples(i).go_count = columns(find(spikes >= 0));
    samples(i).fr_go = samples(i).go_count / interval;
end

