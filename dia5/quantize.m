function R = quantize(frs)
    % cuantificar los firing-rates según las siguientes categorías
    % r = 1 si 0 < FR <= 20
    % r = 2 si 20 < FR <= 30
    % r = 3 si 30 < FR

    R = zeros(size(frs));
    for i = 1:prod(size(frs))
        fr = frs(i);
        if fr <= 20
            R(i) = 1;
        elseif fr <= 30
            R(i) = 2;
        else
            R(i) = 3;
        end
    end
end

