function rasterplot(data, y, markersize)
    xmin = min(data);
    xmax = max(data);
    dx = xmax - xmin;
    plot(data, y*ones(size(data)), '+', 'markersize', markersize);
    line([xmin-dx*.05 xmax+dx*.05], [y y], 'color', 'w');
    plot(data, y*ones(size(data)), '+', 'markersize', 0);
end

