function Rsh = shuffle(R, dir)
    Rsh = R;
    for d = 1:8
        idx_d = find(dir == d);
        for i = 1:3
            idx_sh = idx_d(randperm(rows(idx_d)));
            Rsh(idx_d, i) = R(idx_sh, i);
        end
    end
end

