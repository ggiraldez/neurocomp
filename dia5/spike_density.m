function [t, sd] = spike_density(spikes)
    sigma = 15/1000;
    a = 3;
    dt = 1/1000;

    tmin = min(spikes);
    tmax = max(spikes);

    % generar el tren de spikes compatible con el kernel
    t = [tmin:dt:tmax];
    y = zeros(size(t));
    for i = 1:rows(spikes)
        ts = spikes(i);
        idx = find(t > ts-dt/2 & t < ts+dt/2);
        y(idx) = 1;
    end

    edges = [-a*sigma:dt:a*sigma];
    kernel = normpdf(edges, 0, sigma);

    % realizar la convolución
    sd = conv(y, kernel);

    % recalcular el eje de tiempos teniendo en cuenta la convolución
    tmin = tmin - dt * columns(kernel) / 2;
    tmax = tmax + dt * columns(kernel) / 2;
    t = [tmin:dt:tmax];
    t = t(1:columns(sd));
end

