l = load('contdata_sim2.mat');

figure
subplot(4,2,1);
plot(abs(l.data))

%linea divisoria: plot(ones(size(l.data)) .* m * k,'r-')

m = median(abs(l.data));

for k = 2:8
    x = k + 5;
    final = zeros(size(l.data,1),1);
    spikes = find(l.data > (m*x));
    for s = spikes
        final(s) = l.data(s);
    end
    subplot(4,2,k)
    plot(final)
    title(['filtrando spikes mayores a mediana(abs(datos)) * ', num2str(x) ])
end
