function clusterize(spikes , number_of_clusters)
    [ind, ~] = kmeans(spikes, number_of_clusters);

    figure;
    for i = 1:number_of_clusters
        filtered_spikes = ind == i;

        subplot(number_of_clusters, 1, i);
        plot(spikes(filtered_spikes,:)','b-')
    end
end

