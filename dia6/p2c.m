load('data_sim1.mat');

m = median(abs(l.data));

x = 12;
spikes = find(l.data > (m*x));
final = zeros(50,size(spikes,2));

k = 1;
for i = 1:size(spikes,2)
    s = spikes(i);
    if i < size(spikes,2) & s+1 == spikes(i+1)
        continue
    end
    for j = 1:50
        final(j, k) = l.data(s-13+j);
    end
    k = k + 1;
end
final = final(1:50, 1:k-1);
plot(final, 'b');
clusterize(final',2);
