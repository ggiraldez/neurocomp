load('data_EEG.mat');
load('variable_samps.mat');
subject = 1;
channel = 29;

for s = 1:nSubjs
    subplot(4,3,s)
    title(sprintf('Sujeto #%d', s));
    hold on
    plot(samps,squeeze(ERP(s,1,channel,:)))
    plot(samps,squeeze(ERP(s,2,channel,:)),'r')
    hold off
end


figure
for s = 1:2
    subplot(2,1,s)
    title(sprintf('Potencial evocado, sujeto:#%d', s));
    
    xlabel('Tiempo (ms)');
    ylabel('Potencial (Hz)');
    hold on
    plot(samps,squeeze(ERP(s,1,channel,:)));
    plot(samps,squeeze(ERP(s,2,channel,:)),'r');
    
    legend('Target','Distractor');

    hold off
end