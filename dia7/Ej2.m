load('data_EEG.mat');
load('variable_samps.mat');

distractor = 2;
OZ_channel = 29;

figure
imagesc(squeeze(ERP(:,distractor,OZ_channel,:)))

figure
hold on
xlabel('Tiempo (ms)');
ylabel('Potencial (Hz)');
title('Potenciales en condicion "Distractor"');
plot(samps,squeeze(ERP(:,distractor,OZ_channel,:))')
hold off