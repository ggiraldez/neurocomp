load('data_EEG.mat');
load('variable_samps.mat');

target = 1;
distractor = 2;
OZ_channel = 29;

n = size(ERP,4);
targets = zeros(n,1);
distractors = zeros(n,1);

for i = 1:n
    targets(i) = mean(ERP(:,target,OZ_channel,i));
    distractors(i) = mean(ERP(:,distractor,OZ_channel,i));
end
figure
hold on

title('grand average sobre todos los sujetos');
xlabel('Tiempo (ms)');
ylabel('Potencial (Hz)');
plot(samps,targets)
plot(samps,distractors,'r')
legend('Target','Distractor');
hold off
