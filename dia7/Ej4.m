load('data_EEG.mat');
load('variable_samps.mat');

target = 1;
distractor = 2;

figure
p = 1;
for chan = {'Pz' 'Cz' 'Fz' 'PO8'}
    subplot(2,2,p);
    title(strcat({'Canal '}, chan));
    p=p+1;

    channel = find(ismember(channel_locations,chan));
    
    n = size(ERP,4);
    targets = zeros(n,1);
    distractors = zeros(n,1);

    for i = 1:n
        targets(i) = mean(ERP(:,target,channel,i));
        distractors(i) = mean(ERP(:,distractor,channel,i));
    end
    hold on
    title(['promedio para canal: ',chan]);
    xlabel('Tiempo (ms)');
    ylabel('Potencial (Hz)');
    p1 = plot(samps,targets);
    p2 = plot(samps,distractors,'r');
    legend('target', 'distractor', 'Location', 'NorthWest');
    hold off
end
