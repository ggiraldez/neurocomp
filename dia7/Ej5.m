load('data_EEG.mat');
load('variable_samps.mat');

target = 1;
distractor = 2;

chan = 'Pz';

channel = find(ismember(channel_locations,chan));

n = size(ERP,4);
targets = zeros(n,1);
distractors = zeros(n,1);

for i = 1:n
    targets(i) = sum(ERP(:,target,channel,i))/size(ERP,1);
    distractors(i) = sum(ERP(:,distractor,channel,i))/size(ERP,1);
end
delta = targets-distractors;
threshold = 0.2;
separation_time = samps(find(delta > max(delta)*threshold, 1));
disp(separation_time);
hold on
plot(samps,targets);
plot(samps,distractors,'r');
plot(samps,delta,'k');
plot(samps,max(delta)*threshold,'r');
title('tiempo minimo en el que las dos condiciones se separan');
legend('target', 'distractor', 'Diferencia', sprintf('%.0f%% max', threshold*100), 'Location', 'NW');
hold off
