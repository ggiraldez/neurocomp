load('data_EEG.mat');
load('variable_samps.mat');

target = 1;
distractor = 2;

chan = 'Pz';
channel = find(ismember(channel_locations,chan));
 
n = size(ERP,4);
targets = zeros(n,1);
stargets = zeros(n,1);
distractors = zeros(n,1);
sdistractors = zeros(n,1);

for i = 1:n
    targets(i) = mean(ERP(:,target,channel,i));
    stargets(i) = std(ERP(:,target,channel,i))/sqrt(nSubjs);
    distractors(i) = mean(ERP(:,distractor,channel,i));
    sdistractors(i) = std(ERP(:,distractor,channel,i))/sqrt(nSubjs);
end
limitTargets = targets - stargets;
limitDistractors = distractors + sdistractors;

separation_times = samps(find(limitTargets < limitDistractors));
disp(separation_times(size(separation_times,2)));

delta = targets - distractors;
threshold = 0.2;

figure;
hold on
errorbar(samps, targets, stargets, stargets);
errorbar(samps, distractors, sdistractors, sdistractors, 'r');
plot(samps,limitTargets,'k','LineWidth',2);
plot(samps,limitDistractors,'k','LineWidth',2);
plot(samps,delta,'k-','LineWidth',3);
plot(samps,max(delta)*threshold,'g--','LineWidth',2);
legend('target', 'distractor', 'Delta', sprintf('%.0f%% max', threshold*100), 'Location', 'NW');
title('tiempo minimo en el que las dos condiciones se separan');
xlabel('Tiempo (ms)');
ylabel('Potencial (Hz)');
hold off
