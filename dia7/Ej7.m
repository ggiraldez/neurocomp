load('data_EEG.mat');
load('variable_samps.mat');

target = 1;
distractor = 2;

chan = 'Pz';

channel = find(ismember(channel_locations,chan));

n = size(ERP,4);
targets = zeros(n,1);
distractors = zeros(n,1);

for i = 1:n
    targets(i) = sum(ERP(:,target,channel,i))/size(ERP,1);
    distractors(i) = sum(ERP(:,distractor,channel,i))/size(ERP,1);
end


for i = 1:n
    [p,r] = ranksum(targets(1:i), distractors(1:i),'alpha',0.05);
    if(r == 0)
        disp(samps(i));
        break;
    end
end
