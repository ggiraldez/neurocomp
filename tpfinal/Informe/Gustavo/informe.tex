\documentclass[11pt, a4paper]{article}
\usepackage[left=1.7cm,right=1.7cm,top=2cm, bottom=2cm]{geometry}
\usepackage{graphicx}
\usepackage[utf8]{inputenc}
\usepackage[spanish]{babel}
\usepackage{caratula}
\usepackage{hyperref}
\usepackage[section]{placeins}

\begin{document}

\input{caratula.tex}
\newpage

\section{Objetivo del trabajo}

El trabajo consiste en la comparación de distintos mecanismos de detección de
\emph{bursts} en la actividad neuronal. 

Para ello, comparamos los siguientes algoritmos:

\begin{itemize}
  \item Jon Kleinberg's burst detection algorithm (Jon Kleinberg, 2002) \cite{kleinb}
  \item Poisson spike train analysis (Hanes, Thompson \& Shall, 1995)   \cite{matlab}
  \item Burst onset detector (Hanes, Thompson \& Schall, 1995)  \cite{pburst}
\end{itemize}

Estos algoritmos toman como entrada una secuencia de instantes en los que
ocurrieron disparos de neuronas (\emph{spike trains}) e intentan inferir los
intervalos de tiempo en los que el comportamiento ``normal'' cambió aumentando
la frecuencia (\emph{burst}).

\section{Desarrollo}

El algoritmo de Kleinberg no es un algoritmo diseñado específicamente para ser
usado en neurociencias. La aplicación original es de análisis y detección de
patrones de uso de determinadas palabras en texto escrito. Se basa en la
utilización de un autómata probabilístico que modela un proceso de Markov. En
cada estado se determina probabilísticamente el intervalo de tiempo esperado
hasta el siguiente evento (spike). Los bursts entonces son reportados según los
intervalos de tiempo en que el autómata se encuentra en un estado distinto del
inicial. La implementación utilizada corresponde a Jeff Binder y está escrita
en legunaje R.

Ambos algoritmos de Hanes, Thompson y Schall fueron especialmente construidos
para el análisis de trenes de spikes en un estudio realizado sobre los FEF
(frontal eye fields) y SEF (supplementary eye fields) de macacos. El primero
basado en un trabajo de Legendy y Salcman de 1985 compara los trenes de spikes
con uno teórico que responde a una distribución de Poisson usando como base el
promedio del total de la muestra. La implementación de este algoritmo es en el
lenguaje C. Por último, el segundo algoritmo de Schall está implementado para
Matlab y utiliza una idea similar al anterior, buscando porciones del tren de
spikes cuyos intervalos inter-spike no se corresponden con una distribución de
Poisson de la frecuencia promedio de toda la muestra.


\subsection{Ajuste de los parámetros de los algoritmos}

La implementación en C del algoritmo de Schall de análisis de tren de spike
tiene un parámetro de configuración que permite ajustar la sensibilidad del
mismo. El parámetro denominado \emph{significance level} permite indicar un
límite superior al \emph{surprise index} de los bursts reportados. Todo burst
cuyo índice supere el límite configurado no será reportado por el
algoritmo. De esta forma es posible calibrar el algoritmo para que filtre
bursts espúreos.

\begin{figure}[htb!]
\center
\includegraphics[scale=0.8]{sign-level}
\caption{Efectos de la variación del parámetro significance level para el
algoritmo de Schall (C) con trenes de spike de base 6 Hz y burst de 15 Hz}
\label{fig:signlevel}
\end{figure}

En la figura \ref{fig:signlevel} se puede apreciar el efecto de distintos
niveles de significancia para el caso de un rate base de 6 Hz y burst de 15 Hz.
Para el resto de los experimentos realizados en este trabajo, dicho parámetro
se fijó en un valor de 0.01.

El algoritmo de Kleinberg también tiene 2 parámetros de ajuste: $s$ que
representa la base de $s^{-i}$ (siendo $i$ el estado del autómata) cuyo valor
es proporcional al intervalo inter-spike esperado, y $\gamma$ que escala el
costo de realizar un cambio de estado (ie. de detectar un burst). Un aumento
del valor de $s$ significa que el aumento de frecuencia deberá ser mayor para
que el algoritmo detecte un burst, y un aumento del parámetro $\gamma$
requerirá que dicho aumento de frecuencia se sostenga durante más tiempo para ser
reportado como burst.

Los valores predeterminados son $s = 2$ y $\gamma = 1$ y no fue necesario
modificarlos para obtener resultados satisfactorios en los experimentos
realizados.


\subsection{Experimentos}

Para realizar una comparación cualitativa de los 3 algoritmos, sintetizamos
trenes de spikes usando una distribución de Poisson para generar los
intervalos inter-spike, e introducimos bursts cambiando arbitrariamente el
parámetro de la Poissoniana en los intervalos deseados.

Luego alimentamos los algoritmos con cada una de las 50 muestras generadas y
graficamos en un rasterplot los trenes de spikes y los intervalos que cada
algoritmo detectó como burst.

Realizamos tres experimentos diferentes, variando la frecuencia de disparo base
y la frecuencia de disparo del burst.

\begin{enumerate}
\item Frecuencia base 0.6 Hz y burst entre 4 y 6 segundos a 6 Hz
\item Frecuencia base 6 Hz y burst entre 4 y 6 segundos a 15 Hz
\item Frecuencia base 6 Hz y burst entre 4 y 6 segundos a 30 Hz
\end{enumerate}


\subsection{Resultados}

Podemos ver a continuación en las figuras \ref{fig:exp1}, \ref{fig:exp2} y
\ref{fig:exp3} los rasterplots conteniendo para cada experimento las 50
muestras sintetizadas y el resultado de correr los tres algoritmos en
cada muestra.

\begin{figure}[htb!]
\center
\includegraphics[scale=0.8]{exp1}
\caption{Bursts detectados para el experimento 1 (base 0.6 Hz, burst de 6 Hz)}
\label{fig:exp1}
\end{figure}

\begin{figure}[htb!]
\center
\includegraphics[scale=0.8]{exp2}
\caption{Bursts detectados para el experimento 2 (base 6 Hz, burst de 15 Hz)}
\label{fig:exp2}
\end{figure}

\begin{figure}[htb!]
\center
\includegraphics[scale=0.8]{exp3}
\caption{Bursts detectados para el experimento 3 (base 6 Hz, burst de 30 Hz)}
\label{fig:exp3}
\end{figure}


\section{Conclusiones}

Como puede verse en los rasterplot de la sección resultados, todos los
algoritmos funcionan muy bien para los trenes de spikes generados
sintéticamente en prácticamente todos los casos.

Cabe destacar sin embargo el excelente desempeño del algoritmo de Kleinberg que
a pesar de no haber sido diseñado específicamente para este uso, y sin
necesidad de ajuste de sus parámetros, puede adaptarse perfectamente para este
tipo de análisis.


\begin{thebibliography}{1}

  \bibitem{kleinb} Bursty and Hierarchical Structure in Streams, Jon Kleinberg,
    2002 \\ 
    \url{http://www.cs.cornell.edu/home/kleinber/bhs.pdf} \\
    Burst detection algorithm (R programming language), \\
    \url{http://cran.r-project.org/web/packages/bursts/index.html}

  \bibitem{matlab} Relationship of pressaccadic activity in frontal eye field
    and supplementary eye field to saccade initiation in macaque: Poisson spike
    train analysis, 1995 \\
    \url{http://www.psy.vanderbilt.edu/faculty/schall/pdfs/reofpres.pdf} \\
    Poisson spike train analysis (C language implementation), \\
    \url{http://www.psy.vanderbilt.edu/faculty/schall/scientific-tools/}

  \bibitem{pburst} Relationship of pressaccadic activity in frontal eye field
    and supplementary eye field to saccade initiation in macaque: Poisson spike
    train analysis, 1995 \\
    \url{http://www.psy.vanderbilt.edu/faculty/schall/pdfs/reofpres.pdf} \\
    Burst onset detector (Matlab implementation), \\
    \url{http://www.psy.vanderbilt.edu/faculty/schall/scientific-tools/}


\end{thebibliography}

\end{document}

