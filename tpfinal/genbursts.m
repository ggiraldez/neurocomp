source('bursts.m');

function offsets = generate_from(bursts)
 	offsets = [];
 	for burst = bursts'
 		offsets = [offsets, genpoiss(burst(1),burst(2),burst(3))];
 	end
 	offsets = round(offsets .* 1000);
end

offsets = generate_from(bursts);

file = fopen('poisson.txt','w');
fprintf(file,'%6.2f\n',offsets);
fclose(file);



