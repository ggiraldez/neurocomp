function spikes = genpoiss(Ti, Tf, FR)

    T = (Tf-Ti)*1000;
    lambda = 1/FR*1000;

    isis = poissrnd(lambda, 1, ceil(T/lambda));
    spikes = [Ti];

    for i = 1:size(isis, 2)
        next_spike = spikes(i) + isis(i)/1000;
        if next_spike >= Tf
            break
        end
        spikes(i+1) = next_spike;
    end

end

