#!/bin/bash
cd $(dirname $0)

# echo "data = [" > res.txt
# for j in {1..50}; do
#   echo -n "$j " >> res.txt
#   echo "bursts = [[0,4,0.6];[4,6,6];[6,8,0.6]]" > bursts.m
#   make
#   echo ";"
# done
# echo "]" >> res.txt
# octave plot_result.m

echo "data = [" > res.txt
for j in {1..50}; do
  echo -n "$j " >> res.txt
  echo "bursts = [[0,4,6];[4,6,15];[6,8,6]]" > bursts.m
  make
  echo ";"
done
echo "]" >> res.txt
octave plot_result.m


