function list = generate_list_for(matrix)
  list = zeros(1,1000);
  
  if matrix == 0
    matrix = []
  end
  
  for i = 1:size(matrix,1)
    burst = matrix(i,:);
    init_time = burst(1);
    end_time = burst(2);
    list((ceil(init_time/10)):(ceil(end_time/10))) = 1;
  end

end

source('output.txt');
#R,C,M are filled. 

source('bursts.m');

bursts_list = zeros(1,1000);

baseRate = min(bursts(:,3))
for burst = bursts'
  init_time = burst(1) * 1000;
  end_time = burst(2) * 1000;
  
  if baseRate < burst(3)
    bursts_list(((init_time/10) + 1):((end_time/10) + 1)) = 1;
  end
end
bursts_list = bursts_list(1:1000);

r_list = generate_list_for(R);
m_list = generate_list_for(M);
c_list = generate_list_for(C);

resC = sum(abs(bursts_list - c_list));
resM = sum(abs(bursts_list - m_list));
resR = sum(abs(bursts_list - r_list));
res = [resC, resM, resR]
save -ascii -append res.txt res

