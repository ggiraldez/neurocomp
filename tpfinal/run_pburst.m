file = fopen('./poisson.txt', 'r');
offsets = fscanf(file,'%f');
fclose(file);


[BOB, EOB, SOB]=pburst(offsets, 0, 3000);

start_times = offsets(BOB);
end_times = offsets(EOB);

disp([start_times,end_times]);
