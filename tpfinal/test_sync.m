spikes1 = round([genpoiss(1, 2, 7), genpoiss(2, 3, 40), genpoiss(3,10,7)] .* 1000);
spikes2 = round([genpoiss(1, 5, 10), genpoiss(5, 6, 40), genpoiss(6,10,10)] .* 1000);

[r,a] = event_sync(spikes1, spikes2)
