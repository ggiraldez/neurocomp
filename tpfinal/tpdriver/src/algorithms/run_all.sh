#!/bin/bash

scriptdir=$(dirname $0)
echo "Executing algorithms over $1"
echo -ne "schall.m:\t "
octave -q $scriptdir/schall.m "$1" 0 10000
echo -ne "schall.c:\t "
$scriptdir/schall-c "$1" 0 10000
echo -ne "kelimberg.r:\t "
Rscript $scriptdir/kleimberg.r "$1"
