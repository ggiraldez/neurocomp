(ns tpdriver.algorithms
  [:require
   [clojure.string :as string]]
  [:use
   incanter.core
   [clojure.java.shell :only [sh]]])

(defn parse-int [s]
   (Integer. (re-find  #"\d+" s )))

(defn as-int-tuple [algorithm-name algorithm-output]
  (let [output (string/split algorithm-output #"\s+" )]
    (map parse-int (filter (comp not empty?) output))))

(defn bursts-schall-m [file]
  (as-int-tuple "schall-m"
    ((sh "octave" "-q" "src/algorithms/schall.m" file "0" "10000") :out )))

(defn bursts-schall-c 
  ([file]
   (as-int-tuple "schall-c"
                 ((sh "src/algorithms/schall-c" file "0" "10000") :out )))
  ([file significance]
   (as-int-tuple "schall-c"
                 ((sh "src/algorithms/schall-c" file "0" "10000" (str significance)) :out ))))

(defn bursts-kleimberg [file]
  (as-int-tuple "kleimberg"
    ((sh "Rscript" "src/algorithms/kleimberg.r" file) :out )))
