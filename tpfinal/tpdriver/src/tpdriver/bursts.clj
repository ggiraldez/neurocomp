(ns tpdriver.bursts)
(use '(incanter core charts stats))

(defn poisson-spikes
  ([lambda] (poisson-spikes lambda 0))
  ([lambda n] (cons n (lazy-seq (poisson-spikes lambda (+ n (sample-poisson 1 :lambda lambda)))))))

; generates spikes from 'start' milliseconds to 'end' milliseconds
(defn spikes [start end rate]
  (drop 1 (take-while
    (partial > end)
    (poisson-spikes rate start))))

; params should be in ms.
(defn train-for [lambda-base lambda-burst burst-start burst-end interval-start interval-end]
  (concat
    (spikes interval-start burst-start lambda-base)
    (spikes burst-start burst-end lambda-burst)
    (spikes burst-end interval-end lambda-base)))

; burst-factory returns a function that creates bursts
; example:  ((burst-factory 0 10) {:base 0.6 :burst-rate 100 :interval [4 6] :mixtures 2})
(defn burst-factory
  [interval-start interval-end]
  (fn [{base :base burst-rate :burst-rate [burst-start burst-end] :interval mixtures :mixtures :or {mixtures 1}}]
    (sort
      (reduce (comp distinct concat) ;combine multiple spike trains into one.
        (repeatedly mixtures ;generates multiple spike trains
          #(train-for
            (/ 1000 base)
            (/ 1000 burst-rate)
            (* burst-start 1000)
            (* burst-end 1000)
            (* interval-start 1000)
            (* interval-end 1000)))))))



