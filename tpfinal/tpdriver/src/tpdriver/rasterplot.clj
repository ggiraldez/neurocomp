(ns tpdriver.rasterplot
  [:use [incanter core charts processing]])
 

;; --------------------------------------------------------------------------
;; Data structures and related functions
;;

(def default-rasterplot-style
  {:padding 20
   :train-height 14
   :background [255 255 255]
   :stroke [0 0 0]})

(def default-train-style
  {:spike-height 6
   :stroke [0 0 0]})

(def default-ann-style
  {:stroke [255 0 0]})

(defn style 
  ([] (style {})) 
  ([{:keys [stroke weight]}] {:stroke stroke :weight weight}))

(defn rasterplot []
  {:trains [] :style-map {} :style default-rasterplot-style})

(defn spike-train [spikes]
  {:spikes spikes})

(defn add-spike-train [rasterplot spike-train]
  (let [trains (:trains rasterplot)
        new-trains (conj trains spike-train)]
    (assoc rasterplot :trains new-trains)))

(defn make-simple-rasterplot [trains]
  (let [r (rasterplot)] 
    (reduce #(add-spike-train %1 (spike-train %2)) r trains)))

(defn train-style [stroke-color]
  {:spike-height 5
   :stroke stroke-color})

(defn ann-style [stroke-color]
  {:stroke stroke-color})



;; --------------------------------------------------------------------------
;; Rendering functions
;;

;; TODO: refactor this pattern into a macro
(defn- apply-stroke [sketch color]
  (if (vector? color)
    (let [[r g b] color]
      (.stroke sketch r g b))
    (.stroke sketch color)))

(defn- apply-background [sketch color]
  (if (vector? color)
    (let [[r g b] color]
      (.background sketch r g b))
    (.background sketch color)))


;; draw train of spikes
(defn- draw-spikes [sketch spikes scale-x height]
  (doseq [s spikes]
    (let [x (* s scale-x)]
      (line sketch x 0 x height))))

;; draw time axis
(defn- draw-timeline [sketch x1 x2 begin end ticks]
  (let [line-y 0
        tick-h 5
        label-y 15]
    (doto sketch
      (text-size 10)
      (line x1 line-y x2 line-y)
      (fill 0)
      (text-align CENTER)
      )
    ;; draw ticks
    (doseq [t (range begin end ticks)]
      (let [dx (- x2 x1)
            dt (- end begin)
            x (+ x1 (/ (* t dx) dt))]
        (line sketch x line-y x (+ line-y tick-h))
        (text sketch (str (/ t 1000)) x label-y)))
    ;; last tick (which is excluded from range)
    (line sketch x2 line-y x2 (+ line-y tick-h))
    (text sketch (str (/ end 1000)) x2 label-y)))

;; draw spike train annotations
(defn- draw-annotation [sketch ann scale-x style]
  (let [stroke-color (:stroke style)] 
    (doseq [[t1 t2] (partition 2 ann)]
      (let [x1 (* scale-x t1) 
            x2 (* scale-x t2)] 
        (doto sketch 
          (apply-stroke stroke-color)
          (line x1 0 x2 0)
          (line x1 0 x1 1)
          (line x2 0 x2 1)))))) 

(defn- draw-annotations [sketch annotations scale-x style-map]
  (let [n (count annotations)]
    (doseq [[ann-key ann-data] annotations :when (not (nil? ann-data))]
      (let [style (ann-key style-map default-ann-style)]
        (doto sketch
          (translate 0 -2)
          (draw-annotation ann-data scale-x style))))))

;; draw the spike train with annotations
(defn- draw-spike-train [sketch train scale-x style-map]
  (let [spikes (:spikes train)
        annotations (:annotations train {})
        style (:style train (:train style-map default-train-style))
        spike-height (:spike-height style 5)
        stroke-color (:stroke style 0)]
    (doto sketch
      (apply-stroke stroke-color)
      (draw-spikes spikes scale-x spike-height)
      (draw-annotations annotations scale-x style-map))))

;; draw legend
(defn- draw-legend [sketch legend width style-map]
  (let [n (count legend)
        dx (/ width n)]
    (text-align sketch LEFT)
    (doseq [[i [ann-key ann-label]] (map vector (range n) legend)]
      (let [style (ann-key style-map default-ann-style)
            x (* i dx)]
        (doto sketch
          (apply-stroke (:stroke style))
          (line x 5 (+ 15 x) 5)
          (text ann-label (+ 20 x) 10))))))

;; apply padding translation and return reduced viewport size
(defn- apply-padding 
  ([sketch padding] (apply-padding sketch padding padding))
  ([sketch padding-x padding-y] (let [width (width sketch)
                                      height (height sketch)
                                      new-width (- width (* 2 padding-x))
                                      new-height (- height (* 2 padding-y))]
                                  (. sketch translate padding-x padding-y)
                                  [new-width new-height])))

;; calculate the natural size of the rasterplot
(defn natural-size [rasterplot]
  (let [style (:style rasterplot default-rasterplot-style)
        padding (:padding style 20)
        timeline-height 30
        legend-height 30
        width 500
        train-height (:train-height style 10)
        ntrains (count (:trains rasterplot))
        height (+ (* 2 padding) (* ntrains train-height) timeline-height legend-height)]
    [width height]))

;; produce a processing sketch with the rendered rasterplot
(defn render-rasterplot [rasterplot]
  (let [trains (:trains rasterplot)
        start-t (:start-t rasterplot 0)
        end-t (:end-t rasterplot 10000)
        style-map (:style-map rasterplot {})
        style (:style rasterplot default-rasterplot-style)
        padding (:padding style 20)
        train-height (:train-height style 10)
        legend (:legend rasterplot)]

    (sketch 
      (setup []
             (let [[w h] (natural-size rasterplot)]
               (doto this
                 (size w h) 
                 ;no-loop
                 (framerate 1)
                 smooth)))
      (draw []
            (let [dt (- end-t start-t)
                  [width height] (apply-padding this padding)
                  scale-x (/ width dt)
                  ntrains (count trains)
                  stroke-color (:stroke style 0)
                  background-color (:background style 255)]
              (doto this
                (apply-background background-color)
                (apply-stroke stroke-color))

              (doseq [[i train] (map vector (range ntrains) trains)]
                (doto this 
                  push-matrix 
                  (translate 0 (* train-height i))  
                  (draw-spike-train train scale-x style-map)  
                  pop-matrix)) 

              (doto this
                (apply-stroke stroke-color)
                (translate 0 (* train-height ntrains)) 
                (draw-timeline 0 width start-t end-t (/ dt 10)))
              
              (if (not (nil? legend))
                (doto this
                  (translate 0 20) 
                  (draw-legend legend width style-map))))))))

;; view the rasterplot 
(defn view-rasterplot [rasterplot] 
  ((view (render-rasterplot rasterplot) :size (natural-size rasterplot))))

