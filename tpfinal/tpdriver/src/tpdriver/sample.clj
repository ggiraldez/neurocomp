(ns tpdriver.sample
  [:require
   [tpdriver.bursts :as bursts]
   [tpdriver.algorithms :as algorithms]
   [tpdriver.rasterplot :as rp]
   [clojure.set :as set]
   [clojure.contrib.java-utils :as utils]
   [clojure.string :as string]]
  [:use
   incanter.core
   [clojure.java.shell :only [sh]]])


;; --------------------------------------------------------------------------
;; Spike train generation
;;

(def spike-train-factory (bursts/burst-factory 0 10))

(def template-6-15 {:base 6 :burst-rate 15 :interval [4 6]})
(def template-6-30 {:base 6 :burst-rate 30 :interval [4 6]})
(def template-06-6 {:base 0.6 :burst-rate 6 :interval [4 6]})


;; --------------------------------------------------------------------------
;; Spike train I/O
;;

(defn write-spike-train [spike-train filename]
  (spit filename (str (string/join \newline spike-train) \newline)))

(defn to-file
  "Save a clojure form to a file"
  [object file]
  (with-open [w (java.io.FileWriter. file)]
    (print-dup object w)))

(defn from-file
  "Load a clojure form from file."
  [file]
  (with-open [r (java.io.PushbackReader. (java.io.FileReader. file))]
    (read r)))

(defn name-from-template [template]
  (let [mixtures (or (template :mixtures) 1)
        base (template :base)
        burst (template :burst-rate)
        intervals (template :interval)]
    (str mixtures "-" base "-" burst "-" intervals ".txt")))


;; --------------------------------------------------------------------------
;; Algorithms evaluation methods
;;

(defn run-algorithms-on [filename]
  (let [kleimb (algorithms/bursts-kleimberg filename)
        schall-c (algorithms/bursts-schall-c filename)
        schall-m (algorithms/bursts-schall-m filename)]
    (zipmap [:kleimb :schall-c :schall-m] [kleimb schall-c schall-m])))

;; run callback function (with a filename parameter pointing to the spike train
;; file) repetitions times generating each time a spike train using
;; template-for-burst as the template
(defn generate-and-run [template-for-burst repetitions callback]
  (let [filename "poisson.txt"]
    (repeatedly repetitions
                #(let [train (spike-train-factory template-for-burst)]
                   (write-spike-train train filename)
                   (assoc (callback filename) :spikes train)))))

(defn repeated-results-for [template-for-burst repetitions]
  (generate-and-run template-for-burst repetitions (fn [filename] (run-algorithms-on filename))))

;; --------------------------------------------------------------------------
;; Caching and showing results
;;

(defn cached-result [filename func]
  (when (not (.exists (clojure.contrib.java-utils/file filename)))
    (to-file (func) filename)) 
  (from-file filename))

(defn from-file-or-generate [template]
  (let [filename (name-from-template template)]
    (cached-result filename (fn [] (repeated-results-for template 50)))))

(defn results-6-15 [] (from-file-or-generate template-6-15))
(defn results-6-30 [] (from-file-or-generate template-6-30))
(defn results-06-6 [] (from-file-or-generate template-06-6))

(defn results-06-6-mixing-10 []
  (from-file-or-generate {:base 0.6 :burst-rate 6 :interval [4 6] :mixtures 10}))


;; --------------------------------------------------------------------------
;; Results
;;

(defn measure-latency [burst-inteval burst-detected]
  (let [spected (first burst-inteval)
        detected (first burst-detected)]
    (- detected spected)))

(defn xor [seq1 seq2]
  (let [s1 (set seq1)
        s2 (set seq2)]
    (set/difference (set/union s1 s2) (set/intersection s1 s2))))

(defn build-range [intervals]
  (let [ranges (map #(range (first %1) (second %1))(partition 2 intervals))]
    (reduce concat ranges)))

(defn meassure-precision [burst-intevals burst-detected]
  (let [expected (build-range burst-intevals)
        detected (build-range burst-detected)]
    (count (xor expected detected))))

(defn efficiency-for [algorithm results]
  (reduce + (map (fn [result] (meassure-precision [4000 6000] (result algorithm))) results)))


;; (defn result-for-kleimb [spike-train]
;;   (let [filename "poisson.txt"]
;;     (write-spike-train spike-train filename)
;;     (algorithms/bursts-kleimberg filename)))
;;
;; (defn sample-1-measuring-precision [spike-train]
;;   (meassure-precision [4000 6000] (result-for-kleimb spike-train)))

;;
;; (def trains (for [i (range 50)] (gen-spike-train)))
;; (def precs (for [t trains] (sample-1-measuring-precision t)))
;;



;; --------------------------------------------------------------------------
;; Rasterplots 
;;

(defn- transform-result [result]
  (->
    (rp/spike-train (:spikes result))
    (assoc :annotations (dissoc result :spikes))))

(def default-style-map {:train (rp/train-style 128)
                        :kleimb (rp/ann-style [255 0 0])
                        :schall-c (rp/ann-style [0 192 0])
                        :schall-m (rp/ann-style [0 0 255])})

(def default-legend {:kleimb "Kleinberg"
                     :schall-c "Schall (C)"
                     :schall-m "Schall (Matlab)"})

(defn make-rasterplot-from-results 
  ([results] (make-rasterplot-from-results results default-style-map default-legend))
  ([results style-map legend] (let [r (assoc (rp/rasterplot) :style-map style-map :legend legend)]
                                (reduce #(rp/add-spike-train %1 (transform-result %2)) r results))))

(defn view-rasterplot [rasterplot]
  (let [applet (rp/render-rasterplot rasterplot)
        size (rp/natural-size rasterplot)]
    (incanter.core/view applet :size size)
    applet))

(defn view-results [results]
  (view-rasterplot (make-rasterplot-from-results results)))

;;
;; (require '[tpdriver [sample :as sample] [rasterplot :as rp]] :reload)
;; (rp/view-rasterplot (sample/make-rasterplot-from-results (sample/results-6-30)))
;;

;; --------------------------------------------------------------------------
;; Other experiments 
;;

(defn results-sign-level []
  (cached-result "schall-c-sign-level.txt" 
                 (fn [] (generate-and-run template-6-15 25
                                          (fn [train-file] 
                                            {:sign-0-01 (algorithms/bursts-schall-c train-file 0.01)
                                             :sign-0-1 (algorithms/bursts-schall-c train-file 0.1)
                                             :sign-1 (algorithms/bursts-schall-c train-file 1)})))))

(def sign-level-style-map {:train (rp/train-style 128)
                           :sign-0-01 (rp/ann-style [192 0 0])
                           :sign-0-1 (rp/ann-style [0 192 0])
                           :sign-1 (rp/ann-style [0 0 192])})

(def sign-level-legend {:sign-0-01 "0.01"
                        :sign-0-1 "0.1"
                        :sign-1 "1"})

(defn view-sign-level-plot []
  (let [rasterplot (make-rasterplot-from-results (results-sign-level) sign-level-style-map sign-level-legend )] 
    (view-rasterplot rasterplot)))
